# @commonshost/cli 🐿

Command line interface for the Commons Host service.

## Installation

### Global Installation

Registers the command in the system PATH. Convenient for command line use.

```
npm install --global @commonshost/cli
```

## Local Installation

Adds the package to the `package.json` `devDependencies`. Useful when deploying using [`npm scripts`](https://docs.npmjs.com/misc/scripts). Avoids conflicts between multiple projects using incompatible versions of the command.

```
npm install --development @commonshost/cli
```

## Usage

Run `commonshost` if you globally installed. Run `./node_modules/.bin/commonshost` if you locally installed.

### Create an account

```
$ commonshost signup

? Email address: johnw@example.net
? Username: babayaga
? Password: ***************
 ✔ Registering account
 ✔ Creating new authentication token
 ✔ Saving credentials
```

### Use an existing account

```
$ commonshost login

? Username or email address: babayaga
? Password: [hidden]
 ✔ Authenticating
 ✔ Saving token
```

### Deploy a static web site

```
$ commonshost deploy --domain example.net --root ./ --confirm

To cancel, press Ctrl+C.

Deploying:

   Directory:  /Users/johnw/blog/public
   File count: 14
   Total size: 24 kB
   Options:    default
   URL:        https://example.net

 ✔ Uploading

Deployment successful!
```

## Commands

### Command: `signup`

Alias: `register`

Register a new account.

#### `--username "someone@example.com"`

Email or username for the account.

Optional. If supplied the prompt for this value will be skipped.

#### `--password "secret"`

Secret password key for the account.

Optional. If supplied the prompt for this value will be skipped.

### Command: `login`

Alias: `signin`

Authenticates the account credentials. Only one account is signed-in at a time.

#### `--email "someone@example.com"`

Email for the account.

If supplied as an option, the prompt for this value will be skipped.

#### `--username "janedoe"`

Username for the account.

If supplied as an option, the prompt for this value will be skipped.

#### `--password "secret"`

Secret password key for the account.

If supplied as an option, the prompt for this value will be skipped.

### Command: `logout`

Alias: `signout`

Clears the access token. Does not affect published sites.

### Command: `whoami`

Display currently authenticated username or email.

### Command: `deploy`

Aliases: `publish`, `serve`, `sync`, `upload`

Transfer static files and configuration options to the service.

#### `--domain "example.com"`

Domain name for the site.

If the domain is not specified as a command line option, the `./CNAME` file is checked for a valid domain. This is a text file that has a domain name as its first line.

If not specified, and no `CNAME` file can be loaded, an interactive prompt will suggest a few random domains or allow entry of a custom domain name. Make sure the DNS records for any custom domain are set up correctly. The suggested random domains are automatically configured. Upon successful deployment the domain name is saved in the `CNAME` file.

#### `--options "options.json"`

Path to a valid [`@commonshost/configuration`](https://gitlab.com/commonshost/configuration) options file containing either a server configuration or just a single host. If a server configuration contains multiple hosts, the host with matching domain is used, otherwise the first host is used.

Can be JSON or a JavaScript file that exports a configuration object.

#### `--root "./some/path"`

Path to a directory containing static files to upload.

#### `--confirm`

Skip the manual deployment confirmation.

### Command: `list`

Aliases: `ls`, `ll`, `la`, `l`

Display a list of all sites owned by the currently authenticated user.

### Command: `config [get|set|delete|reset]`

Aliases: `configure`, `configuration`, `setting`, `settings`, `option`, `options`

Edit the user settings.

Specify the setting name and value as `--my.setting.name "my setting value"`. Dot-notation is used for nested property names.

Multiple settings may be configured in sequence. `--foo=bar --lol=true`.

Settings can be removed by specifying the `delete` subdommand.

The entire settings file is cleared using the `reset` subcommand.

#### Example Configuration

```js
{
  // See the `login` command
  username: 'babayega',
  access_token: 'eyJ0eX...',
  // Base URL of the CDN API
  api: {
    origin: 'https://api.commons.host:8888'
  },
  // Auth0 tenant settings
  auth0: {
    client_id: 'WX1Kgy0NWL069OaiaCJLQE3OL0gqzq2e',
    domain = 'https://http2.au.auth0.com',
    database = 'Username-Password-Authentication',
    audience = 'https://commons.host/',
    scope = 'openid deploy'
  }
}
```

## See Also

- [@commonshost/configuration](https://gitlab.com/commonshost/configuration)
- [@commonshost/server](https://gitlab.com/commonshost/server)
- [@commonshost/core](https://gitlab.com/commonshost/core)
- [@commonshost/edge](https://gitlab.com/commonshost/edge)

## Colophon

Made with ❤️ by Sebastiaan Deckers in 🇸🇬 Singapore.
