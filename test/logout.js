const test = require('blue-tape')
const {prepare} = require('./helpers')
const {runSync} = require('./helpers/runner')

test('logout', async (t) => {
  await prepare('logged_in.json')
  runSync('logout')
  const done = runSync('config --username --access_token')
  t.is(done.stdout, 'username undefined\naccess_token undefined\n')
})
