const test = require('blue-tape')
const {prepare} = require('./helpers')
const {runSync} = require('./helpers/runner')

test('report logged in user', async (t) => {
  await prepare('logged_in.json')
  const done = runSync('whoami')
  t.is(done.stdout, 'johnw@example.net\n')
})

test('report nothing when logged out', async (t) => {
  await prepare('logged_out.json')
  const done = runSync('whoami')
  t.is(done.stdout, '')
})
