const test = require('blue-tape')
const {prepare} = require('./helpers')
const {run} = require('./helpers/runner')
const {createServer} = require('http')
const path = require('path')
const {readFileSync} = require('jsonfile')
const {toASCII} = require('punycode/')
const Busboy = require('busboy')
const {promisify} = require('util')

test('deploy', (t) => new Promise(async (resolve) => {
  t.plan(12)
  const server = createServer(async (request, response) => {
    t.is(request.url, `/v2/sites/${toASCII('💩.example.net')}`)
    t.is(request.method, 'PUT')
    const validToken = 'test/fixtures/valid_token.json'
    const fixture = path.resolve(process.cwd(), validToken)
    const {access_token: expectedBearer} = readFileSync(fixture)
    t.is(request.headers.authorization, `Bearer ${expectedBearer}`)
    const busboy = new Busboy({headers: request.headers, preservePath: true})
    busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
      t.is(fieldname, 'directory')
      switch (filename) {
        case 'stuff/foo.bar':
          t.is(mimetype, 'application/octet-stream')
          break
        case 'index.html':
          t.is(mimetype, 'text/html')
          break
        default:
          t.fail('Wrong filename')
      }
      file.resume()
    })
    busboy.on('field', (fieldname, val, fieldnameTruncated, valTruncated) => {
      t.is(fieldname, 'configuration')
      t.is(val, '{"foo":"bar"}')
    })
    busboy.on('finish', () => {
      response.writeHead(200)
      response.end(JSON.stringify({
        type: 'site-deploy',
        domain: toASCII('💩.example.net'),
        isNewDomain: true,
        hasNewFiles: true,
        hasNewConfiguration: true
      }))
    })
    request.pipe(busboy)
  })
  await promisify(server.listen.bind(server))(0)

  // JWT signing key: "secret"
  await prepare('valid_token.json', {
    api: {origin: `http://localhost:${server.address().port}`}
  })

  const root = 'test/fixtures/deploy/root'
  const options = 'test/fixtures/deploy/options_object.js'
  const domain = '💩.example.net'
  const command = `deploy --root ${root} --options ${options} --domain ${domain} --confirm`
  const {stdout, stderr} = await run(command)
  t.false(stderr)
  t.ok(stdout.includes('Deployment successful'))
  t.ok(stdout.includes('https://💩.example.net'))

  server.close(resolve)
}))

test('deploy when options file is an async function', async (t) => {
  const server = createServer(async (request, response) => {
    request.on('end', () => {
      response.end(JSON.stringify({
        type: 'site-deploy',
        domain: 'example.com'
      }))
    })
    request.resume()
  })
  await promisify(server.listen).call(server, 0)

  // JWT signing key: "secret"
  await prepare('valid_token.json', {
    api: {origin: `http://localhost:${server.address().port}`}
  })

  const root = 'test/fixtures/deploy/root'
  const options = 'test/fixtures/deploy/options_function.js'
  const domain = 'example.net'
  const command = `deploy --root ${root} --options ${options} --domain ${domain} --confirm`
  const {stdout, stderr} = await run(command)
  t.false(stderr)
  t.ok(stdout.includes('Deployment successful'))
  t.ok(stdout.includes('https://example.net'))

  await promisify(server.close).call(server)
})

test('deploy fails when options file is missing', async (t) => {
  await prepare('valid_token.json', {
    api: {origin: `http://localhost:0`}
  })

  const root = 'test/fixtures/deploy/root'
  const options = 'test/fixtures/deploy/options.does_not_exist'
  const domain = 'example.net'
  const command = `deploy --root ${root} --options ${options} --domain ${domain} --confirm`
  const {stderr} = await run(command)
  t.ok(stderr.includes('Error: Configuration file not found'))
})

test('deploy fails when options file has no export', async (t) => {
  await prepare('valid_token.json', {
    api: {origin: `http://localhost:0`}
  })

  const root = 'test/fixtures/deploy/root'
  const options = 'test/fixtures/deploy/options_missing_export.js'
  const domain = 'example.net'
  const command = `deploy --root ${root} --options ${options} --domain ${domain} --confirm`
  const {stderr} = await run(command)
  t.ok(stderr.includes('TypeError: Configuration is empty'))
})

test('deploy fails when options file does not export an object', async (t) => {
  await prepare('valid_token.json', {
    api: {origin: `http://localhost:0`}
  })

  const root = 'test/fixtures/deploy/root'
  const options = 'test/fixtures/deploy/options_string.js'
  const domain = 'example.net'
  const command = `deploy --root ${root} --options ${options} --domain ${domain} --confirm`
  const {stderr} = await run(command)
  t.ok(stderr.includes('TypeError: Configuration is not an object'))
})
