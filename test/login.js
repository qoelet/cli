const test = require('blue-tape')
const {prepare} = require('./helpers')
const {run} = require('./helpers/runner')
const {createServer} = require('http')
const {promisify} = require('util')
const read = require('read-all-stream')

const USERNAME = 'foo'
const PASSWORD = 'bar'

test('login to Auth0', async (t) => {
  t.plan(6)

  const server = createServer(async (request, response) => {
    t.is(request.url, '/oauth/token')
    t.is(request.method, 'POST')
    t.is(request.headers['content-type'], 'application/json')
    const body = await read(request)
    const json = JSON.parse(body)
    t.deepEqual(json, {
      connection: 'Username-Password-Authentication',
      client_id: 'abc123',
      grant_type: 'password',
      audience: 'https://auth0.example.net/',
      scope: 'openid deploy',
      username: USERNAME,
      password: PASSWORD
    })
    response.writeHead(200)
    response.end(JSON.stringify({}))
  })
  await promisify(server.listen.bind(server))(0)

  await prepare('auth0_mock.json', {
    auth0: {domain: `http://localhost:${server.address().port}`}
  })

  const command = `login --username ${USERNAME} --password ${PASSWORD}`
  const {stdout, stderr} = await run(command)
  t.false(stderr)
  t.true(stdout.includes('Saving token [completed]'))

  await promisify(server.close).call(server)
})
