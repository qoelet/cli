const {read} = require('../options')
const jwt = require('jsonwebtoken')

module.exports.loadingCredentials = async () => {
  const {
    access_token: accessToken,
    api: {origin = 'https://api.commons.host:8888'} = {}
  } = await read()
  if (accessToken === undefined) {
    throw new Error('No authentication token found')
  }
  const {payload: {exp}} = jwt.decode(accessToken, {complete: true})
  if (exp * 1e3 < Date.now()) {
    throw new Error('Authentication token expired')
  }
  return {accessToken, origin}
}
