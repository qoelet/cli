const {read, write, filepath} = require('../options')
const omit = require('lodash.omit')
const unset = require('lodash.unset')
const get = require('lodash.get')
const merge = require('lodash.merge')
const flatten = require('flat')

module.exports.command = ['config']
module.exports.aliases = ['configure', 'configuration', 'setting', 'settings', 'option', 'options']
module.exports.desc = 'Edit user settings'
module.exports.builder = (yargs) => {
  yargs
    .example('$0 config get --access_token')
    .example('$0 config set --api.origin=https://api.commons.host:8888')
    .example('$0 config delete --api')
    .epilog(`Configuration: ${filepath}`)
}

module.exports.handler = async function handler (argv) {
  const options = await read()
  const blacklist = ['v', 'version', 'h', 'help', '_', '$0']
  const payload = omit(argv, blacklist)
  const flattened = Object.keys(flatten(payload))
  const subcommand = argv._[1]
  switch (subcommand) {
    case 'rm':
    case 'remove':
    case 'delete':
      for (const dotted of flattened) {
        unset(options, dotted)
      }
      await write(options)
      break

    case 'w':
    case 'set':
    case 'write':
      merge(options, payload)
      await write(options)
      break

    case 'r':
    case 'get':
    case 'read':
    case undefined:
      if (flattened.length === 0) {
        console.log(options)
      } else {
        for (const dotted of flattened) {
          console.log(dotted, get(options, dotted))
        }
      }
      break

    case 'reset':
      await write()
      break
  }
}
